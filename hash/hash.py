# Импортируем библиотеку hashlib
import hashlib

# Задаем название файла
file_name_kesl = 'kesl_11.2.2-6739_amd64.deb'
file_name_kesl_agent = 'klnagent64_12.0.0-60_amd64.deb'
# Корректный хэш дистрибутива 
original_md5_kesl = '86CCFAEF677ADFF8394F048C1005853C'
original_md5_agent = 'CADE393511DA78BAC2198EBE34E6FA9F'  

#Открываем файл самого дистрибутива
with open(file_name_kesl, 'rb') as file_to_check:
    data = file_to_check.read()
    md5_returned_kesl = hashlib.md5(data).hexdigest()
    print(md5_returned_kesl)
#Открываем файл агента
with open(file_name_kesl_agent, 'rb') as file_to_check:
    data = file_to_check.read()
    md5_returned_agent = hashlib.md5(data).hexdigest()
    print(md5_returned_agent)

#Вывод результатов проверки контрольных сумм дистрибутива
if original_md5_kesl.lower() == md5_returned_kesl:
    print("MD5 дистрибутива соотвествует.")
else:
    print("MD5 дистрибутива не соотвествует.")


#Вывод результатов проверки контрольных сумм агента
if original_md5_agent.lower() == md5_returned_agent:
    print("MD5 агента соотвествует.")
else:
    print("MD5 агента не соотвествует.")



